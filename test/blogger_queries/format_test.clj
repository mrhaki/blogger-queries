(ns blogger-queries.format-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-queries.format :as format]))

(deftest format-percentage
  (testing "Format score value <= 1 as is"
    (is (= (format/format-percentage 0.23) "23%"))
    (is (= (format/format-percentage 0.239182) "24%"))
    (is (= (format/format-percentage 1.0) "100%")))
  (testing "Format score value > 1 as 1"
    (is (= (format/format-percentage 1.2) "100%"))))

(deftest make-url-https
  (testing "URL already starts with https should not change"
    (is (= (format/make-url-https "https://test") "https://test")))
  (testing "URL starging with http must be replaced with https"
    (is (= (format/make-url-https "http://test") "https://test"))))

(comment
  
  ,)

