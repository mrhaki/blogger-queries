(ns blogger-queries.blog-test
  (:require [clojure.test :refer [deftest testing is]]
            [datalevin.core :as d]
            [blogger-queries.blog :as blog]
            [blogger-queries.system :as system]))

(defn- upsert-blog
  [conn]
  (let [post {:id        "7356010244958013577"
              :published "2021-03-05T22:44:00+01:00"
              :updated   "2021-03-05T22:48:55+01:00"
              :url       "http://blog.mrhaki.com/2021/03/gradle-goodness.html"
              :title     "Post Title"
              :content   "<p>Test Blog Post</p>"
              :labels    ["Gradle"]
              :etag      "\"dGltZXN0YW1\""}]
    (d/transact! conn [post])))

(deftest get-blogs
  (testing "Get all blogs from database"
    (let [started-system (system/start (system/test-system))
          conn (:connection (:database started-system))]
      (upsert-blog conn)
      (is (= (blog/get-blogs conn) [{:id "7356010244958013577"
                                     :url "http://blog.mrhaki.com/2021/03/gradle-goodness.html"
                                     :title "Post Title"
                                     :published "2021-03-05T22:44:00+01:00"
                                     :labels ["Gradle"] 
                                     :content "<p>Test Blog Post</p>"}]))
      (system/stop started-system))))
