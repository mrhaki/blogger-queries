(ns blogger-queries.solr-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-queries.solr :as solr])
  (:import (java.util ArrayList LinkedHashMap)
           (org.apache.solr.client.solrj.response QueryResponse)
           (org.apache.solr.common.util NamedList)
           (org.apache.solr.common SolrInputDocument SolrDocumentList SolrDocument)))

(deftest solr
  (let [container (solr/create-core-container)
        server (solr/create container)
        post {:id        "7356010244958013577"
              :published "2021-03-05T22:44:00+01:00"
              :updated   "2021-03-05T22:48:55+01:00"
              :url       "http://blog.mrhaki.com/2021/03/gradle-goodness.html"
              :title     "Post Title"
              :content   "<p>Test Blog Post</p>"
              :labels    ["Gradle"]
              :etag      "\"dGltZXN0YW1\""}
        doc (solr/create-doc post)]

    (solr/clean! server)
    (solr/add! server [doc])
    (solr/commit! server)

    (testing "Count by label"
      (is (= (solr/count-by-label server "Gradle") 1)))

    (testing "Query by label"
      (let [found (solr/->clojure (solr/query-by-label server "Gradle" 1))
            response (:response found)
            doc (first (:docs response))]
        (is (= (:numFound response) 1))
        (is (= (:start response) 0))
        (is (= (:id doc) "7356010244958013577"))
        (is (= (:link doc) "http://blog.mrhaki.com/2021/03/gradle-goodness.html"))
        (is (= (:title doc) "Post Title"))
        (is (= (:label doc) ["Gradle"]))))

    (testing "Query related posts"
      (let [found (solr/->clojure (solr/query-related-posts server "7356010244958013577"))
            response (:response found)
            doc (first (:docs response))
            more-like-this (:7356010244958013577 (:moreLikeThis found))]
        (is (= (str (:maxScore response)) "0.30685282"))
        (is (= (:id doc) "7356010244958013577"))
        (is (= (:link doc) "http://blog.mrhaki.com/2021/03/gradle-goodness.html"))
        (is (= (:title doc) "Post Title"))
        (is (= (str (:score doc)) "0.30685282"))
        (is (= (:numFound more-like-this) 0))
        (is (= (:start more-like-this) 0))
        (is (= (:docs more-like-this) []))
        (is (= (:maxScore more-like-this) 0.0))))

    (solr/shutdown container)))

(deftest ->clojure
  (testing "Convert default"
    (is (= (solr/->clojure "Test") "Test")))

  (testing "Convert NamedList"
    (let [list (doto (NamedList.) (.add "key" "value"))]
      (is (= (solr/->clojure list) {:key "value"}))))

  (testing "Convert SolrResponse"
    (let [list (doto (NamedList.) (.add "key" "value"))
          resp (doto (QueryResponse.) (.setResponse list))]
      (is (= (solr/->clojure resp) {:key "value"}))))

  (testing "Convert SolrInputDocument"
    (let [doc (doto (SolrInputDocument.) (.addField "key" "value" 0.1))]
      (is (= (solr/->clojure doc) {:key "value"}))))

  (testing "Convert SolrDocument"
    (let [doc (doto (SolrDocument.) (.addField "key" "value"))]
      (is (= (solr/->clojure doc) {:key "value"}))))

  (testing "Convert SolrDocumentList"
    (let [doc (doto (SolrDocument.) (.addField "key" "value"))
          list (doto (SolrDocumentList.) (.add doc))]
      (is (= (solr/->clojure list) {:numFound 0 :start 0 :docs [{:key "value"}]})))
    (let [doc (doto (SolrDocument.) (.addField "key" "value"))
          list (doto (SolrDocumentList.) (.add doc) (.setMaxScore (float 12.0)))]
      (is (= (solr/->clojure list) {:numFound 0 :start 0 :docs [{:key "value"}] :maxScore 12.0}))))

  (testing "Convert ArrayList"
    (let [list (doto (ArrayList.) (.add "Test"))]
      (is (= (solr/->clojure list) ["Test"]))))

  (testing "Convert LinkedHashMap"
    (let [map (doto (LinkedHashMap.) (.put "key" "value"))]
      (is (= (solr/->clojure map) {:key "value"})))))