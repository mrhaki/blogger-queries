(ns blogger-queries.file-test
  (:require [clojure.test :refer [deftest is run-tests]]
            [clojure.java.io :refer [file make-parents]]
            [blogger-queries.file :as file]))

(deftest save-file
  (let [filename "test.txt"
        output-dir "./build/"
        file (file output-dir filename)]
    (make-parents file)
    (file/save! output-dir filename "Data") 
    (is (= (slurp file) "Data"))))

(comment
  (run-tests)
  ,)

