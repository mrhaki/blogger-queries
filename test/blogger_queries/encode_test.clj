(ns blogger-queries.encode-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-queries.encode :as encode]))

(deftest encode
  (testing "Encode given value with UTF-8"
    (is (= (encode/encode "Clojure:Goodness") "Clojure%3AGoodness"))))