(ns blogger-queries.json-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-queries.json :as json]))

(deftest label-posts
  (testing "Create JSON output for label posts"
    (is (= (json/label-posts [{:link "http://localhost" :title "Title"}])
           "[{\"url\":\"https:\\/\\/localhost\",\"title\":\"Title\"}]"))))

(deftest related-posts
  (testing "Create JSON output for releated posts"
    (is (= (json/related-posts [{:link "http://localhost" :title "Title" :score 0.3889}]) 
           "[{\"url\":\"https:\\/\\/localhost\",\"title\":\"Title\",\"score\":\"39%\"}]"))))

(deftest json-callback
  (testing "Create JSON callback"
    (is (= (json/json-callback "cb" "{\"user\": \"test\"}") "cb({\"user\": \"test\"});"))))

(comment
  ,)

