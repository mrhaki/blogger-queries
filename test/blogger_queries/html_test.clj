(ns blogger-queries.html-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-queries.html :as html]))

(deftest label-posts
  (testing "Create HTML unordered list for label posts"
    (is (= (html/label-posts "test" [{:link "http://localhost" :title "Title"}]) 
           "<ul class=\"label-posts\" id=\"label-posts-list-test\"><li><a href=\"https://localhost\">Title</a></li></ul>"))))

(deftest related-posts
  (testing "Create HTML unordered list for related posts"
    (is (= (html/related-posts "blog-id" [{:link "http://localhost" :title "Title" :score 0.39933544}]) 
           "<ul class=\"related-posts\" id=\"related-posts-list-blog-id\"><li><a href=\"https://localhost\">Title<em>(Matching score 40%)</em></a></li></ul>"))))