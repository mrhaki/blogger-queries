(ns blogger-queries.core
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [clojure.tools.logging :as log]
            [cprop.core :refer [load-config]]
            [blogger-queries.system :as system]
            [blogger-queries.solr :as solr :refer [->clojure]]
            [blogger-queries.blog :as blog]
            [blogger-queries.file :as file]
            [blogger-queries.html :as html]
            [blogger-queries.json :as json]))

(def blog-labels ["Awesome:Asciidoctor"
                  "Clojure:Goodness"
                  "DataWeave:Delight"
                  "Gradle:Goodness"
                  "Grails:Goodness"
                  "Groovy:Goodness"
                  "Groovy:Grassroots"
                  "Java:Joy"
                  "Micronaut:Mastery"
                  "PlantUML:Pleasantness"
                  "Ratpacked"
                  "Spocklight"
                  "Spring:Sweets"])

(defn add-posts-to-solr
  [solr posts]
  (log/info "Add posts to SOLR")
  (->clojure (solr/clean! solr))
  (->clojure (solr/add! solr (->> posts
                                  (map solr/create-doc))))
  (->clojure (solr/commit! solr)))

(defn- find-label-posts
  [solr label]
  (let [count (solr/count-by-label solr label)]
    (->> (->clojure (solr/query-by-label solr label count))
         :response
         :docs
         (sort #(compare (:published %2) (:published %1))))))

(defn generate-label-posts
  [output-dir solr]
  (log/info "Generating label posts")
  (let [labels blog-labels]
    (doseq [label labels]
      (let [posts (find-label-posts solr label)]
        (file/save! output-dir (str "labels-" label ".html") (html/label-posts label posts))
        (file/save! output-dir (str "labels-" label ".jsonp") (json/json-callback "showLabelPosts" (json/label-posts posts)))))))

(defn generate-related-posts
  [output-dir solr posts]
  (log/info "Generating related posts")
  (doseq [post posts]
    (let [blog-id (:id post)
          related-posts (->> (->clojure (solr/query-related-posts solr blog-id))
                             :moreLikeThis
                             nfirst
                             first
                             :docs)]
      (file/save! output-dir (str "post-" blog-id ".html") (html/related-posts blog-id related-posts))
      (file/save! output-dir (str "post-" blog-id ".jsonp") (json/json-callback "showRelatedPosts" (json/related-posts related-posts))))))

(defn get-config []
  (let [local-config (load-config :file "~/.blogger.edn")]
    {:database {:storage-dir (-> local-config :database :storage-dir)}
     :output   {:storage-dir "./output"}}))

(defn -main
  [& _]
  (let [config (get-config)
        system (component/start (system/production-system config))
        solr (:solr-server (:solr system))
        connection (:connection (:database system))
        storage (:location (:storage system))
        posts (blog/get-blogs connection)]
    (add-posts-to-solr solr posts)
    (generate-label-posts storage solr)
    (generate-related-posts storage solr posts)
    (component/stop system)))

(comment
  (-main))
  
  
