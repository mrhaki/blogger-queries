(ns blogger-queries.format
  (:require [clojure.string :as str])
  (:import (java.text NumberFormat)
           (java.util Locale)))

(defn format-percentage
  "Format score as percentage."
  [^double score]
  (let [formatter (NumberFormat/getPercentInstance Locale/US)
        ^double score-value (if (> score 1) 1 score)]
    (.format formatter (Double/valueOf score-value))))

(defn make-url-https
  "Use https protocol for url."
  [url]
  (if (str/starts-with? url "http:")
    (str/replace url "http" "https")
    url))  

(comment
  (format-percentage 0.38910)
  (format-percentage 1.1)
  (make-url-https "http://blog.mrhaki")
  ,)
