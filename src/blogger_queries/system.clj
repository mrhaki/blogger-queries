(ns blogger-queries.system
  (:require [com.stuartsierra.component :as component]
            [datalevin.core :as d]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [blogger-queries.solr :as solr]))

(defrecord Database [storage-dir connection]
  component/Lifecycle

  (start
    [component]
    (log/infof "Starting database %s" storage-dir)
    (if connection
      component
      (let [schema {:id     {:db/valueType :db.type/string
                             :db/unique    :db.unique/identity}
                    :url    {:db/valueType :db.type/string
                             :db/unique    :db.unique/identity}
                    :labels {:db/valueType   :db.type/string
                             :db/cardinality :db.cardinality/many}}]
        (assoc component :connection (d/get-conn storage-dir schema)))))

  (stop
    [component]
    (log/info "Stopping database")
    (if (d/closed? connection)
      (assoc component :connection nil)
      (assoc component :connection (d/close connection)))))

(defn- new-database
  "Create new database component."
  [storage-dir]
  (map->Database {:storage-dir storage-dir}))

(defrecord Solr [core-container solr-server]
  component/Lifecycle

  (start
    [component]
    (log/info "Starting SOLR")
    (if core-container
      component
      (let [container (solr/create-core-container)]
        (assoc component :core-container container
               :solr-server (solr/create container)))))

  (stop
    [component]
    (log/info "Stopping SOLR")
    (if core-container
      (assoc component :core-container (solr/shutdown core-container) :solr-server nil)
      (assoc component :core-container nil :solr-server nil))))

(defn- new-solr
  "Create new SOLR component"
  []
  (map->Solr {}))

(defrecord OutputStorage [output-dir location]
  component/Lifecycle

  (start
    [component]
    (let [dir (io/as-file output-dir)]
      (log/info "Clean output directory")
      (doseq [file (.listFiles (io/as-file "./output"))]
        (.delete file))
      (.mkdirs dir)
      (assoc component :location dir)))

  (stop
    [component]
    (assoc component :location nil)))

(defn new-output-storage
  "Create new output storage component."
  [output-dir]
  (map->OutputStorage {:output-dir output-dir}))

(defn production-system
  "Create system map for production."
  [config]
  (let [{database-config :database
         output-config   :output} config]
    (component/system-map
     :database (new-database (:storage-dir database-config))
     :solr (new-solr)
     :storage (new-output-storage (:storage-dir output-config)))))

(defn test-system
  "Create system map used for testing."
  []
  (component/system-map
   :database (new-database "./build/test/blogger-db")
   :solr (new-solr)
   :storage (new-output-storage "./build/test/output")))

(defn start
  "Performs side effects to initialize the system, acquire resources,
  and start it running. Returns an updated instance of the system."
  [system]
  (component/start system))

(defn stop
  "Performs side effects to shut down the system and release its
  resources. Returns an updated instance of the system."
  [system]
  (component/stop system))
