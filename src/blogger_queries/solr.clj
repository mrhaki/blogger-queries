(ns blogger-queries.solr
  (:require [blogger-queries.encode :refer [encode]])
  (:import (org.apache.solr.client.solrj SolrQuery SolrResponse)
           (org.apache.solr.client.solrj.embedded EmbeddedSolrServer)
           (org.apache.solr.core CoreContainer$Initializer)
           (org.apache.solr.common SolrInputDocument SolrDocumentList SolrDocument)
           (org.apache.solr.common.util NamedList)
           (java.util ArrayList LinkedHashMap)
           (java.time LocalDateTime)
           (java.time.format DateTimeFormatter)))

(defmulti ->clojure class)

(defmethod ->clojure NamedList [^NamedList response]
  (into {} (for [[k v] response] [(keyword k) (->clojure v)])))

(defmethod ->clojure ArrayList [obj]
  (mapv ->clojure obj))

(defmethod ->clojure SolrDocumentList [^SolrDocumentList obj]
  (merge
    {:numFound (.getNumFound obj)
     :start (.getStart obj)
     :docs (mapv ->clojure (iterator-seq (.iterator obj)))}
    (when-let [ms (.getMaxScore obj)]
      {:maxScore ms})))

(defmethod ->clojure SolrDocument [^SolrDocument obj]
  (reduce
    (fn [acc f]
      (assoc acc (keyword f) (->clojure (.getFieldValue obj f))))
    {}
    (.getFieldNames obj)))

(defmethod ->clojure SolrResponse [^SolrResponse obj]
  (->clojure (.getResponse obj)))

(defmethod ->clojure SolrInputDocument [^SolrInputDocument obj]
  (reduce
    (fn [acc o]
      (assoc acc (keyword o) (.getFieldValue obj o)))
    {}
    (.getFieldNames obj)))

(defmethod ->clojure LinkedHashMap [obj]
  (into {} (for [[k v] obj] [(keyword k) (->clojure v)])))

(defmethod ->clojure :default [obj]
  obj)

(defn create-core-container
  "Create core SOLR container."
  []
  (let [initializer (CoreContainer$Initializer.)]
    (. initializer initialize)))

(defn create 
  "Create SOLR server with container"
  [core-container]
  (EmbeddedSolrServer. core-container ""))

(defn shutdown 
  "Shutdown SOLR container."
  [core-container]
  (.shutdown core-container))

(defn- convert-date-time
  [date-time]
  (let [parsed-date (LocalDateTime/parse date-time DateTimeFormatter/ISO_OFFSET_DATE_TIME)
        formatted-date (.format (DateTimeFormatter/ofPattern "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") parsed-date)]
    formatted-date))

(defn create-doc
  "Create SOLR input document with id, link, title, published, label and content fields."
  [{:keys [id url title published labels content]}]
  (doto (SolrInputDocument.)
    (.addField "id" id)
    (.addField "link" url)
    (.addField "title" title)
    (.addField "published" (convert-date-time published))
    (.addField "label" (ArrayList. (map encode labels)))
    (.addField "content" content)))

(defn query-related-posts
  "Find related posts for a post with given id."
  [solr id]
  (let [solr-query (doto 
                     (SolrQuery.)
                     (.setQuery (str "id:" id))
                     (.setParam "mlt.fl" (into-array ["content,title"]))
                     (.setParam "mlt.count" (into-array ["5"]))
                     (.setParam "mlt.mindf" (into-array ["5"]))
                     (.setParam "mlt" true)
                     (.setParam  "fl" (into-array ["id,title,link,score"])))]
    (.getResponse (. solr query solr-query))))

(defn query-by-label
  "Find all posts for a given label and return the number of posts given by count."
  [solr label count]
  (let [solr-query (doto 
                     (SolrQuery.)
                     (.setRows (int count))
                     (.setQuery  (str "label:" (encode label))))]
    (.getResponse (.query solr solr-query))))

(defn count-by-label
  "Count the number of posts for a given label."
  [solr label]
  (let [solr-query (doto
                     (SolrQuery.)
                     (.setQuery  (str "label:" (encode label))))]
    (->> (->clojure (.getResponse (.query solr solr-query)))
         :response
         :numFound)))

(defn add! 
  "Add document to SOLR."
  [solr docs]
  (.add solr docs))

(defn commit!
  "Commit changes to SOLR."
  [solr]
  (.commit solr))

(defn clean!
  "Remove all documents from SOLR."
  [solr]
  (.deleteByQuery solr "*:*")
  (commit! solr))
