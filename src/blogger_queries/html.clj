(ns blogger-queries.html
  #_{:clj-kondo/ignore [:unresolved-var]}
  (:require [hiccup.core :refer [html]]
            [hiccup.element :refer [link-to]]
            [blogger-queries.encode :refer [encode]]
            [blogger-queries.format :refer [format-percentage make-url-https]]))

(defn label-posts
  "Create unordered list with links to all posts for given label in HTML."
  [label posts]
  (html [:ul {:class "label-posts" :id (str "label-posts-list-" (encode label))}
           (for [post posts]
             [:li #_{:clj-kondo/ignore [:unresolved-var]}
                  (link-to (make-url-https (:link post)) (:title post))])]))

(defn related-posts
  "Create unordered list with links to related posts for all posts in HTML."
  [blog-id posts]
  (html [:ul {:class "related-posts" :id (str "related-posts-list-" blog-id)}
         (for [post posts]
           [:li #_{:clj-kondo/ignore [:unresolved-var]}
                (link-to (make-url-https (:link post)) (:title post) 
                         [:em (str "(Matching score " (format-percentage (:score post)) ")")])])]))

(comment
  (label-posts "Clojure:Goodness" [{:link "http://localhost" :title "Title"}])
  
  (related-posts "100" [{:link "http://localhost" :title "Title" :score 0.39933544}])
  ,)
