(ns blogger-queries.json
  (:require [clojure.data.json :as json]
            [blogger-queries.format :refer [format-percentage make-url-https]]))

(defn label-posts
  "Create JSON array with posts for label."
  [posts]
  (json/write-str (map (fn [{:keys [link title]}] {:url (make-url-https link) :title title}) posts)))

(defn related-posts
  "Create JSON array with related posts."
  [posts]
  (json/write-str (map (fn [{:keys [link title score]}] {:url (make-url-https link) :title title :score (format-percentage score)}) posts)))

(defn json-callback
  "Create JSONP callback structure."
  [callback json]
  (str callback "(" json ");"))

(comment
  (label-posts [{:link "http://localhost" :title "Title"}])
  (related-posts [{:link "http://localhost" :title "Title" :score 0.3889}])
  ,)
