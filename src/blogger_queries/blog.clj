(ns blogger-queries.blog
  (:require [datalevin.core :as d]))

(defn get-blogs
  "Get all blogs from Datalevin database."
  [connection]
  (map first (d/q '[:find (pull ?e [:id :url :title :published :labels :content])
                    :where [?e :id]]
                  (d/db connection))))
