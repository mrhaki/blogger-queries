(ns blogger-queries.encode
  (:import (java.net URLEncoder)))

(defn encode
  "Encode string value with URLEncoder/encode."
  [^String value]
  (URLEncoder/encode value "UTF-8"))

(comment
  (encode "Clojure:Goodness")
  ,)
