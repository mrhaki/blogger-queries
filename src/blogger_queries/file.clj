(ns blogger-queries.file
  (:require [clojure.java.io :refer [file]]))

(defn save!
  "Save content in file with given name in given directory."
  [output-dir filename content]
  (let [file (file output-dir filename)]
    (spit file content)))

(comment
  (save! "./output" "test.txt" "Data")
  ,)
