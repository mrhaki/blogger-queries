(ns user
  (:require [blogger-queries.system :as system]
            [blogger-queries.core :as core]
            [blogger-queries.solr :as solr :refer [->clojure]]
            [blogger-queries.blog :as blog]
            [blogger-queries.html :as html]
            [blogger-queries.json :as json]
            [blogger-queries.encode :refer [encode]]
            [datalevin.core :as d]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer (pprint)]
            [clojure.repl :refer :all]
            [clojure.test :as test]
            [clojure.tools.namespace.repl :refer (refresh refresh-all)])
  (:import (java.net URLEncoder)
           (java.util ArrayList)
           (org.apache.solr.client.solrj SolrQuery)
           (java.time LocalDateTime ZoneOffset)
           (java.time.format DateTimeFormatter)))

(def system nil)

(defn init
  "Constructs the current development system."
  []
  (alter-var-root #'system
                  (constantly (system/test-system))))

(defn start
  "Starts the current development system."
  []
  (alter-var-root #'system system/start))

(defn stop
  "Shuts down and destroys the current development system."
  []
  (alter-var-root #'system
                  (fn [s] (when s (system/stop s)))))

(defn go
  "Initializes the current development system and starts it running."
  []
  (init)
  (start))

(defn reset []
  (stop)
  (refresh :after 'user/go))


(comment
  (reset)
  (go)
  (stop)
  

  (def conn (:connection (:database system)))
  (def blog-post {:id        "7356010244958013577",
                  :published "2021-03-05T22:44:00+01:00",
                  :updated   "2021-03-05T22:48:55+01:00",
                  :url       "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html",
                  :title     "Gradle Goodness: Enabling Preview Features For Java",
                  :content   "<p>Java introduced preview features in the language since Java 12.
                    This features can be tried out by developers, but are still subject to change and can even be removed in a next release.
                    By default the preview features are not enabled when we want to compile and run our Java code.
                    We must explicitly specify that we want to use the preview feature to the Java compiler and Java runtime using the command-line argument <code>--enable-preview</code>.
                    In Gradle we can customize our build file to enable preview features.
                    We must customize tasks of type <code>JavaCompile</code> and pass <code>--enable-preview</code> to the compiler arguments.
                    Also tasks of type <code>Test</code> and <code>JavaExec</code> must be customized where we need to add the JVM argument <code>--enable-preview</code>.</p>
                    <p>Written with Gradle 6.8.3</p><p>Java 11.</p>
                    ",
                  :labels    ["Gradle" "Java"
                              "Gradle 6.8.3"
                              "Gradle:Goodness"
                              "GradleGoodness:Java"
                              "GradleGoodness:Kotlin"],
                  :etag      "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMAo\""})

  (d/transact! conn [blog-post])
  (blog/get-blogs conn)

  (def solr (:solr-server (:solr system)))
  (->clojure (.deleteByQuery solr "*:*"))
  (->clojure (.commit solr))
  (->clojure (.add solr (->> (blog/get-blogs conn)
                             (map solr/create-doc))))

  ;; (. container shutdown)

  (def solr-input-doc (solr/create-doc {:id "my-id" :title "my-title"}))
  (. solr-input-doc getFieldNames)

  (. solr add solr-input-doc)
  (. solr commit)

  (->> (blog/get-blogs conn)
       (take 2))

  (d/q '[:find (pull ?e [*])
         :where
         [?e :title ?title]
         [(.startsWith ?title "DataWeave Delight:")]]
       (d/db conn))

  (d/q '[:find ?id ?url ?title ?labels ?content
         :keys id url title labels content
         :where
         [?e :title ?title]
         [?e :url ?url]
         [?e :content ?content]
         [?e :labels ?labels]
         [?e :id ?id]
         [(.startsWith ?title "DataWeave Delight:")]]
       (d/db conn))

  (->> (d/q '[:find (pull ?e [:id :url :title :published :labels :content])
              :where [?e :id]]
            (d/db conn))
       (map first)
       (take 3))

  (defn get-blogs-with-title-prefix
    [connection title-prefix]
    (d/q '[:find (pull ?e [*])
           :in $ ?blog-title
           :where
           [?e :title ?title]
           [(.startsWith ?title ?blog-title)]]
         (d/db connection)
         title-prefix))


  (defn get-post-by-url
    [conn url]
    (into {} (d/entity @conn [:url url])))

  (def records (get-blogs-with-title-prefix conn "Clojure Goodness:"))

  (first records)
  (def solr-docs (mapv solr/create-doc (get-blogs-with-title-prefix conn "Clojure Goodness:")))

  (def solr-docs (->> (get-blogs-with-title-prefix conn "DataWeave Delight:")
                      (map first)
                      (map solr/create-doc)
                      (.toString (take 1 solr-docs))))
  (count solr-docs)
  (.add solr solr-docs)
  (.commit solr)

  (get-post-by-url conn "http://blog.mrhaki.com/2018/06/groovy-goodness-tuples-with-up-to-9.html")


  (sort #(compare (:id %1) (:id %2)) (:docs (:response (solr/->clojure (solr/query-by-label solr (URLEncoder/encode "Clojure:Goodness" "UTF-8") 10)))))

  (->clojure (solr/query-by-label solr (URLEncoder/encode "Clojure:Goodness" "UTF-8") 5))
  (def result (->clojure (solr/query-by-label solr (URLEncoder/encode "Clojure:Goodness" "UTF-8") 5)))
  (->clojure (solr/query-related-posts solr "1368156197651876108"))
  (->> (->clojure (solr/query-related-posts solr "1368156197651876108"))
       :moreLikeThis
       nfirst
       first
       :docs)

  (sort #(compare (:published %2) (:published %1)) (:docs (:response result)))

  (second (into-array (map #(URLEncoder/encode % "UTF-8") ["MuleSoft" "DataWeave:Core"])))

  (def q (doto (new SolrQuery)
           (.setQuery "id:1")
           (.setParam "facet" true)
           (.setParam "s1" (into-array ["mrhaki" "hubert"]))))
  (bean q)

  (def post {:published "2022-02-16T06:12:00+01:00"
             :id        "100"
             :title     "Clojure 2"
             :link       "https://blog.mrhaki.com/post"
             :content   "Content"
             :labels    ["Clojure:Goodness"]})


  (bean (solr/create-doc post))
  (->clojure (.add solr (solr/create-doc post)))
  (->clojure (.commit solr))

  (html/label-posts "Groovy" [post])

  (html/label-posts "Groovy" [post])
  (json/label-posts [post])
  (json/json-callback "showLabelPosts" (json/label-posts [post]))
  (map (fn [p] {:id (:id p)}) [post])

  (defn find-label-posts
    [solr label]
    (let [count (solr/count-by-label solr label)]
      (->> (->clojure (solr/query-by-label solr label count))
           :response
           :docs
           (sort #(compare (:published %2) (:published %1))))))

  (find-label-posts solr "Java:Joy")
  (->clojure (solr/query-by-label solr "Java:Joy" 10))
  (solr/count-by-label solr "Java:Joy")

  (html/label-posts (encode "Clojure:Goodness") (find-label-posts solr "Clojure:Goodness"))
  (->clojure (solr/query-by-label solr "Clojure:Goodness" 10))

  (let [labels ["Clojure:Goodnes" "DataWeave:Delight"]]
    (doseq [label labels]
      (doseq [post [{:id 1 :title "TITLE"} {:id 2 :title "T2"}]]
        (println label)
        (println post))))

  (println system)
  (println (:solr system))
  (stop)
  (go)
  (reset)

  (println (:storage system))

  (doseq [file (.listFiles (io/as-file "./output"))]
    (.delete file))
  )


