(ns build
  (:require [clojure.tools.build.api :as b]))

(def build-dir "build/")
(def classes-dir (str build-dir "classes/"))

(def basis (delay (b/create-basis {:project "deps.edn"})))

(defn clean
  [_]
  (b/delete {:path build-dir}))

(defn uberjar
  [_]
  (let [uber-file (str build-dir "blogger-queries.jar")]
    (b/copy-dir {:src-dirs ["src" "resources"]
                 :target-dir classes-dir})
    (b/compile-clj {:basis @basis
                    :ns-compile '[blogger-queries.core]
                    :class-dir classes-dir
                    :compile-opts {:direct-linking true}})
    (b/uber {:class-dir classes-dir
             :uber-file uber-file
             :basis @basis
             :main 'blogger-queries.core})))